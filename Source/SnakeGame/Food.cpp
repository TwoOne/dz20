// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Snake.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

/*
void AFood::EatFood(ASnake* Snake, float SpeedBonus, int AddElement)
{
	Snake->AddSnakeElement(AddElement);
	Snake->SetMovementSpeed(SpeedBonus);

}
*/

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		/*
		if (ASnake* Snake = Cast<ASnake>(Interactor))
		{
			int EatIndex = UKismetMathLibrary::RandomIntegerInRange(0, 1);

			switch (EatIndex)
			{
			case 0:
				EatFood(Snake, Snake->GetMovementSpeed() - 0.003f, 2);
				break;

			case 1:
				EatFood(Snake, Snake->GetMovementSpeed() + 0.002f, 1);
				break;

			default:;
			}
		}
		*/
		
		auto Snake = Cast<ASnake>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
		}
		
	}
}

